FROM golang:1.13
COPY main.go .
RUN go build -o main main.go
EXPOSE 8080
CMD ["./main"]
