package main

import (
	"io"
	"net/http"
)

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		_, err := io.WriteString(w, "Hello World\n")
		if err != nil {
			http.Error(w, err.Error(), 500)
		}

	})

	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		panic(err)
	}
}
